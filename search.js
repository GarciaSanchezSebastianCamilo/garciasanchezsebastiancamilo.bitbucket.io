var html = "";
var APIKEY = "AIzaSyAUdSRLXHa2W0BSrMTT5stfMZZ-M7h5IPg";
//var CLIENT_ID = '782477490572-ego65hjb9unha4p1e5atk99ale9p6ohb.apps.googleusercontent.com';
var CLIENT_ID = "782477490572-ego65hjb9unha4p1e5atk99ale9p6ohb.apps.googleusercontent.com";

var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/youtube.readonly';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest", "https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest", "https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"];
var authorizeButton = document.getElementById('authorize-button');      
var signoutButton = document.getElementById('signout-button');

function onClientLoad() {
  gapi.load('client:auth2', initClient);
  //gapi.client.load('youtube', 'v3', onYouTubeApiLoad);
}

function initClient() {
  gapi.client.init({
    apiKey: APIKEY,
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Escuche los cambios de estado de inicio de sesi�n.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Maneja el estado de inicio de sesi�n inicial.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

function updateSigninStatus(isSignedIn) {
  console.log("Desde updateSigninStatus... ");
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    addButton.style.display = 'block';
    
    //datosusuario();
    llamarEventos();
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
    addButton.style.display = 'none';
  }
}

function handleAuthClick(event) {    
  gapi.auth2.getAuthInstance().signIn();
}

/**
*  Sign out usuario al hacer clic en el bot�n.
*/
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
}

/**
*  Fin - Proceso de autenticaci�n. 
*/

/**
* PARTE 2 - Tratando los eventos desde la interfaz de usuario y
* realizar llamadas API. 
**/

var addButton = document.getElementById('addToCalendar');
addButton.onclick = function(){
  var userChoices = getUserInput();
  console.log(userChoices);
  if (userChoices) 
    createEvent(userChoices);
}

function getUserInput(){
  
  var date = document.querySelector("#date").value;
  var startTime = document.querySelector("#start").value;
  var endTime = document.querySelector("#end").value;
  var eventDesc = document.querySelector("#query").value;
  console.log(startTime + " " + endTime);
  // Comprobar que los valores de entrada no esten vac�os
  if (date=="" || startTime=="" || endTime=="" || eventDesc==""){
    alert("Todos los campos deben tener un valor");
    return
  }
  else return {'date': date, 'startTime': startTime, 'endTime': endTime,
               'eventTitle': eventDesc}
}


// Realizar una llamada de API para crear un evento. Dar comentarios al usuario.
function createEvent(eventData) {
  // Primero se crea el recurso que se enviar� al servidor.
    var resource = {
        "summary": eventData.eventTitle,
        "start": {
          "dateTime": new Date(eventData.date + " " + eventData.startTime).toISOString()
        },
        "end": {
          "dateTime": new Date(eventData.date + " " + eventData.endTime).toISOString()
          }
        };
    // Crear la solicitud
    var request = gapi.client.calendar.events.insert({
      'calendarId': 'primary',
      'resource': resource
    });
  
    // Ejecutar la solicitud y hacer algo con respuesta
    request.execute(function(resp) {
      console.log(resp);
      alert("Su evento ha sido agregado");
    });
}

//-----------------------

function onYouTubeApiLoad() {
    gapi.client.setApiKey("AIzaSyBZImKUNBTMob-Ojb-KbTEP-_x2aWtp6dI");
}

function validar(){
    html ="";
    document.getElementById('errores').innerHTML = html;
    var numResultados = document.getElementById('resultados').value;
    numResultados = Number(numResultados);
    console.log(numResultados);
    if(numResultados <0 || numResultados>50){
        
        html += '<div class="alert alert-danger">Busqueda por default, para la proxima introduce un numero mayor a 0 y menor a 50</div>';
        document.getElementById('errores').innerHTML = html;
        numResultados = 100;
    }
    return numResultados;
}

var map;
var marcadoresArray = [];
function initMap() { 
  map = new google.maps.Map(document.getElementById("mapDiv"), { 
    center: {lat: 17.0606433, lng: -96.7319102}, 
    zoom: 4
  }); 

  var infowindow = new google.maps.InfoWindow(); 
  var marker = new google.maps.Marker({ 
    map: map, 
    anchorPoint: new google.maps.Point(0, -29) 
  });
}
function llamarEventos(){

  var bottonsearch = document.getElementById("search");
  bottonsearch.addEventListener("click",search);
  }
  


function search() {
  /*
   var resultados="";
    initMap();
    var divresultados = document.getElementById("results");  
     while(divresultados.hasChildNodes()){
    divresultados.removeChild(divresultados.firstChild);}
  
  */
 initMap();
  $("#itemContainer").html("");
  videos = [];
  var ides = "";
    var a= document.getElementById("query").value;  
    var b= document.getElementById("resultados").value;
   
     // prepare the request
         var request = gapi.client.youtube.search.list({
            part: "snippet,id",
            type: "video",
            q: a,
            maxResults: b,
  
       }).then(function(response){       
         /*
        var resultados = response.result.items;
         for ( var i=0; i<resultados.length; i++){
          var frames = document.createElement("iframe");  
          frames.setAttribute('style',"width:"+  1800/b);
          frames.setAttribute('style',"height:"+ 250/b);
          
           var title=resultados[i].snippet.title;
            var idsvideos=resultados[i].id.videoId;
            frames.setAttribute('src', "https://www.youtube.com/embed/" + idsvideos);
            
            divresultados.appendChild(frames);
  
             listarInfo(idsvideos);
       } */
       var results = response.result;
       for (var i = 0; i < results.items.length; i++) {
          videos.push(results.items[i]);
          buscar_latitud_longitud(results.items[i].id.videoId);//ides
       $("#itemContainer").append("<li><iframe width=\"150\" height=\"100\" src=\"//www.youtube.com/embed/" + results.items[i].id.videoId +"\" frameborder=\"0\" allowfullscreen></iframe></li>");
      //cargando resultados en el itemContainer
      }  
       mostrarpag();
       
         });
  
    }
  
    function mostrarpag() {
      /* initiate the plugin */
        $("div.holder").jPages({
          containerID  : "itemContainer",
          perPage      : 10,
          startPage    : 1,
          startRange   : 1,
          midRange     : 5,
          endRange     : 1
        });
          }
  

function buscar_latitud_longitud(ids){
   console.log("funcion latitud-longitud youtube................................");
   var longitud=0;
    var latitud=0; 
    var request = gapi.client.youtube.videos.list({
      part : 'recordingDetails',
      id : ids
    });
    request.execute(function (response){
      //console.log("Ga");
      //if(response.items !== 'undefined'){
        var elementos = response.items;
      console.log(response.result.items);
      try{
        if(elementos[0].hasOwnProperty('recordingDetails')){
          if(elementos[0].recordingDetails.hasOwnProperty('location')){
            if(elementos[0].recordingDetails.location.hasOwnProperty('latitude')){
              if(elementos[0].recordingDetails.location.hasOwnProperty('longitude')){
                longitud=elementos[0].recordingDetails.location.longitude;
                latitud=elementos[0].recordingDetails.location.latitude;
                console.log(latitud);
                console.log(longitud);
                marcarMarcadores(latitud,longitud);
              }
            }
          }
        }
      }
        catch(error) {
          //console.log(":c");
        } 
      //}
    });
  }

function marcarMarcadores(latitud, longitud){
  console.log("youtube-maps");
  console.log("posicion:"+ latitud , longitud);
var uluru = {lat: latitud, lng: longitud};
   marker = new google.maps.Marker({
    position: {lat: latitud, lng: longitud},
    map: map
  });
/*
function onPlayerReady(event) {
    event.target.playVideo();
}

function eliminarMarcadores(){
  if (marcadoresArray) {
    for (i in marcadoresArray) {
      marcadoresArray[i].setMap(null);
    }
    marcadoresArray.length = 0;
  }
}

function creaMarcador(_titulo, _id_video, _latitud, _longitud){
    var contentString = 
      '<div id="content">'+
        '<h6 id="firstHeading" class="firstHeading" style="margin-bottom: 0;">'+
          _titulo +
        '</h6>'+
        '<div id="bodyContent">'+
          '<iframe src=\"https://www.youtube.com/embed/' + _id_video + '\" width="200" height="auto"></iframe>';
        '</div>'+
      '</div>'
    ;

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    var marker = new google.maps.Marker({
      position: {"lat": _latitud, "lng": _longitud},
      map: map,
      title: _titulo
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });

    
    /*
    marker.addListener('mouseover', function() {
        var player = new YT.Player('player', {
          height: '200',
          width: '300',
          videoId: _id_video,
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
    });
    */

}


var boton = document.getElementById("search");
boton.addEventListener("click",tweets,"false");
/*
var youtubeDiv = document.getElementById('mapDiv');
while (youtubeDiv.firstChild) {
  youtubeDiv.removeChild(youtubeDiv.firstChild);
      }
*/
 function tweets(){
	var cad = document.getElementById("query").value;
	console.log(cad);
	    var cb =new Codebird();
      cb.setConsumerKey ( "e26D1HcnlC1K3oUbNHjV2zdES" , "pBUuXNjyq2sBIqx4w046rgJ3hWEfHqSSHDDq5X84E0YQJ84djT" );
      cb.setToken("3287982397-qla5SjtKvNIseryoP80FN5mupVJyjx01tGLIMDH", "m8UIIraBlztjWJiROtLNr6gulkcMsTL9sxu7fqdc3k0b6");
		
			var cad = document.getElementById("query").value;
		
		objeto = {
			q: cad,
			count:100	
		};

cb.__call (
     "search_tweets" ,
     objeto,
     function( response ) {
				 
				 console.log("tweets..............");
				 
				 for(var i=0;i<response.statuses.length;i++){

		        if(response.statuses[i].coordinates!==null){
						   	var longitud = response.statuses[i].coordinates.coordinates[0];
                var latitud = response.statuses[i].coordinates.coordinates[1];
								console.log("coordenadas " + latitud + " /"  + longitud);
                  //puntear en el mapa los que tengan latitud y longitud
								MapaTweets(latitud,longitud);

							}
							else{
								console.log("No hay coordenadas.Twitter");
							}
		}// fin for
	
}// fin response
)//fin call
}// fin funcion twitter

function MapaTweets(latitud, longitud){
		var image = "twitter.png"
		console.log("posicion:"+ latitud , longitud);
  var uluru = {lat: latitud, lng: longitud};
     marker = new google.maps.Marker({
      position: uluru,
      map: map,
      icon: image
    });
  };



function ubicacion(searchItem) {
    var request = gapi.client.youtube.videos.list({
        part: 'recordingDetails',
        id: searchItem.id.videoId
    });

    request.execute(function (respuesta) {
        
        var titulo_video = searchItem.snippet.title;
        var id_video = searchItem.id.videoId;
        //var canal = searchItem.snippet.channelTitle;
        var fecha = searchItem.snippet.publishedAt;
        html += '<div class="row">';
        html += '<div class="col-md-12">';
        //contenido del video
        html += '<iframe id="'+ id_video + '" src=\"//www.youtube.com/embed/' + id_video + '\" allowfullscreen width="300" height="auto"></iframe>';
        //html += '<div id="'+ id_video + '" src=\"//www.youtube.com/embed/' + id_video + '?enablejsapi=1\" allowfullscreen width="300" height="auto"></iframe>';
        html += '<p><b>Titulo del Video: </b>' + titulo_video + '<br>';
        html += '<b>Fecha de Subida: </b>' + moment(fecha).format('DD/MM/YYYY') + '<br><p>';
        html += '<hr class="hr-danger"/>';//fin del contenedir del video
        html += '</div>';
        html += '</div>';

        var latitud = undefined;
        var longitud = undefined;
        for (var i = 0; i < respuesta.items.length; i++) {
            if(respuesta.items[i].hasOwnProperty("recordingDetails")){
                if(respuesta.items[i].recordingDetails.hasOwnProperty("location")){
                    if(respuesta.items[i].recordingDetails.location.hasOwnProperty("latitude") && 
                       respuesta.items[i].recordingDetails.location.hasOwnProperty("longitude")){
                        if(respuesta.items[i].recordingDetails.location.latitude !== undefined && 
                           respuesta.items[i].recordingDetails.location.longitude !== undefined){
                            latitud = respuesta.items[i].recordingDetails.location.latitude;
                            longitud = respuesta.items[i].recordingDetails.location.longitude;
                            break;
                        }
                    }
                }
            }
        }

        if(latitud === undefined || longitud === undefined){
            console.log("no hay coordenadas");
        }else{
            console.group("Coordenadas"); 
            console.log("Latitud: " + latitud);
            console.log("Longitud: " + longitud);
            console.groupEnd();
            creaMarcador(
                searchItem.snippet.title, 
                id_video, 
                latitud, 
                longitud
            );
        }

        document.getElementById('info').innerHTML = html;
    });
};