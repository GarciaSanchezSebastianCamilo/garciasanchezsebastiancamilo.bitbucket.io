var CLIENT_ID = '219516285401-2dud2arp1cvufll9nt7la2o5b0ihaua1.apps.googleusercontent.com';
var apiKey = 'AIzaSyBZImKUNBTMob-Ojb-KbTEP-_x2aWtp6dI';
var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/calendar';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest", "https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

var authorizeButton = document.getElementById('authorize-button');      
var signoutButton = document.getElementById('signout-button');
      
/** 
*  Función invocada cuando se carga la biblioteca javascript de cliente
*/
function handleClientLoad() {
  console.log("Desde handleClientLoad ...");
  gapi.load('client:auth2', initClient);
}

/**
*  Inicializa la biblioteca de cliente de API y configura los usuarios 
*  de estado de inicio de sesión.
*/
function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Escuche los cambios de estado de inicio de sesión.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Maneja el estado de inicio de sesión inicial.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

/**
*  Se llama cuando el estado de inicio de sesión cambia, para
*  acualizar la interfaz de usuario apropiadamente. Después de 
*  un inicio de sesión, se llama a la API.
*/
function updateSigninStatus(isSignedIn) {
  console.log("Desde updateSigninStatus... ");
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    addButton.style.display = 'block';
    
    datosusuario();

    /*
    //load the calendar client library
    gapi.client.load('calendar', 'v3', function(){ 
      console.log("Calendar library loaded.");
    });
    */
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
    addButton.style.display = 'none';
  }
}

/**
*  Sign in usuario al hacer clic en el botón.
*/
function handleAuthClick(event) {    
  gapi.auth2.getAuthInstance().signIn();
  /*
  gapi.auth.authorize({client_id: CLIENT_ID, scope: SCOPES, immediate: false}, handleAuthResult);
  return false;
  */
}

/**
*  Sign out usuario al hacer clic en el botón.
*/
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();

  // Logout Gmail
  document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:8083";
}

/**
*  Fin - Proceso de autenticación. 
*/

/**
* PARTE 2 - Tratando los eventos desde la interfaz de usuario y
* realizar llamadas API. 
**/

var addButton = document.getElementById('addToCalendar');
addButton.onclick = function(){
  var userChoices = getUserInput();
  console.log(userChoices);
  if (userChoices) 
    createEvent(userChoices);
}

function getUserInput(){
  
  var date = document.querySelector("#date").value;
  var startTime = document.querySelector("#start").value;
  var endTime = document.querySelector("#end").value;
  var eventDesc = document.querySelector("#event").value;
  console.log(startTime + " " + endTime);
  // Comprobar que los valores de entrada no esten vacíos
  if (date=="" || startTime=="" || endTime=="" || eventDesc==""){
    alert("All your input fields should have a meaningful value.");
    return
  }
  else return {'date': date, 'startTime': startTime, 'endTime': endTime,
               'eventTitle': eventDesc}
}


// Realizar una llamada de API para crear un evento. Dar comentarios al usuario.
function createEvent(eventData) {
  // Primero se crea el recurso que se enviará al servidor.
    var resource = {
        "summary": eventData.eventTitle,
        "start": {
          "dateTime": new Date(eventData.date + " " + eventData.startTime).toISOString()
        },
        "end": {
          "dateTime": new Date(eventData.date + " " + eventData.endTime).toISOString()
          }
        };
    // Crear la solicitud
    var request = gapi.client.calendar.events.insert({
      'calendarId': 'primary',
      'resource': resource
    });
  
    // Ejecutar la solicitud y hacer algo con respuesta
    request.execute(function(resp) {
      console.log(resp);
      alert("Your event was added to the calendar.");
    });
}

function datosusuario() {
  gapi.client.request({
    'path': 'https://www.googleapis.com/gmail/v1/users/me/profile'
  }).then(function(response) { // Realizar solicitud a la API
    // Procesar respuesta
    var _pusuario = document.getElementById("idpusuario");
    _pusuario.textContent = "Email: " + response.result.emailAddress;
    
    }, function(reason) { // En caso de error
      console.error('Error: ' + reason.result.error.message);
    });
}